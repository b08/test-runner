import { test } from "../src";

test("throw exception", expect => {
  expect.exception(() => {
    throw new Error();
  });

});

test("throw exception 2", expect => {
  expect.exception(function (): void {
    throw new Error();
  });
});

test("no exception", async expect => {
  expect.exception(() => null);
});


test("throw exception async", async expect => {
  await expect.exception(async () => {
    throw new Error();
  });

});

test("no exception async", async expect => {
  await expect.exception(async () => null);
});
