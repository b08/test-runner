import { test } from "../src";

test("failing test", expect => {
  expect.equal(5, 4);
  expect.true(false);
  expect.false(true);

});

test("fail with deepEqual", async expect => {
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: [1, 2], b: "text1" });
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: [1, 2, 3], b: "text" });
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: [1, 4], b: "text" });
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: "", b: "text" });
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: null, b: "text" });
  expect.deepEqual({ a: [1, 2], b: "text", c: true }, null);
});
