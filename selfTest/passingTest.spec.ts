import { test } from "../src";

test("should pass", async expect => {
  expect.equal(5, 5);
  expect.true(true);
  expect.false(false);
});

test("pass with deepEqual", async expect => {
  expect.deepEqual({ a: [1, 2], b: "text" }, { a: [1, 2], b: "text" });
});
