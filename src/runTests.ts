import * as globby from "globby";
import { getReporters } from "./reporting/getReporter";
import { RunnerOptions } from "./runnerOptions.type";
import { splitExecute } from "./execute/splitExecute";
import { TestResult } from "./types";
import * as os from "os";

const defaultOptions: RunnerOptions = {
  files: ["./**/*.spec.js", "!./node_modules/**/*"],
  reporter: ["verbose", "summary"],
  testTimeout: 2000,
  timeout: 30000
};

export async function runTests(options: RunnerOptions = {}): Promise<TestResult> {
  options = { ...defaultOptions, ...options };
  const files = await globby(options.files, { absolute: true });
  options.threads = determineThreadCount(options.threads, files.length);
  const tests = await splitExecute(files, options);
  const reporters = getReporters(options.reporter);
  const output = reporters.map(r => r.report(tests)).join("");
  return {
    output,
    passed: !tests.some(t => !t.passed)
  };
}

function determineThreadCount(threads: number, filesCount: number): number {
  if (threads > 0) { return threads; }
  const maximum = os.cpus().length;
  const minimum = 1;
  const calculated = Math.ceil((filesCount - 9) / 10); //  1-19 => 1 thread,  20-29 => 2 thread and so on
  const topCap = Math.min(calculated, maximum);
  const bottomCap = Math.max(topCap, minimum); // can't be less than 1 thread
  return bottomCap;
}

