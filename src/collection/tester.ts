import { Assertion, IExpect } from "../types";
import { deepEqual } from "./deepEqual";

export class Tester implements IExpect {
  public assertions: Assertion[] = [];

  public true(value: boolean, customText?: string): void {
    const assertion = { passed: value === true, text: customText || "Value should be true" };
    this.assertions.push(assertion);
  }

  public false(value: boolean, customText?: string): void {
    const assertion = { passed: value === false, text: customText || "Value should be false" };
    this.assertions.push(assertion);
  }

  public equal<T>(value1: T, value2: T, customText?: string): void {
    const text = customText || "Values should match";
    this.assertions.push({ passed: value1 === value2, text, expected: value2, actual: value1 });
  }

  public deepEqual<T>(value1: T, value2: T, customText?: string): void {
    const text = customText || "Values should deeply match";
    const deepAssertion = deepEqual(value1, value2, text);
    this.assertions.push(deepAssertion);
  }

  public exception(code: () => void | Promise<void>, customText?: string): void | Promise<void> {
    const text = customText || "Code should throw an exception ";
    let result: any;
    try {
      result = code();
      if (result && result.then != null) {
        return result
          .then(() => this.assertions.push({ passed: false, text }))
          .catch(() => this.assertions.push({ passed: true, text }));

      }
      this.assertions.push({ passed: false, text });
    } catch (err) {
      this.assertions.push({ passed: true, text });
    }
  }

  public error(err: Error): void {
    this.assertions.push({ passed: false, text: err.message || "Exception was thrown during test" });
  }
}
