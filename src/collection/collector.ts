import { Test } from "../types";
import { RunnerOptions } from "../runnerOptions.type";

export type Cb = (results: Test[]) => void;

export class Collector {
  public options: RunnerOptions;
  private tokens: Set<any> = new Set();
  private callbacks: Cb[] = [];
  private tests: Test[] = [];

  public reset(): void {
    this.tests = [];
    this.tokens = new Set();
    this.callbacks = [];
  }

  public setOptions(options: RunnerOptions): void {
    this.options = options;
  }

  public startTest(token: any): void {
    this.tokens.add(token);
  }

  public finishTest(token: any, test: Test): void {
    this.tests.push(test);
    this.tokens.delete(token);
    if (this.tokens.size === 0) {
      this.callbacks.forEach(cb => cb(this.tests));
      this.callbacks = [];
    }
  }

  public onFinish(cb: Cb): void {
    if (this.tokens.size === 0) {
      cb(this.tests);
    } else {
      this.callbacks.push(cb);
    }
  }
}

export const collector = new Collector();

