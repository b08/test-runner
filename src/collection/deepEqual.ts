import { Assertion } from "../types";

export function deepEqual(a: any, b: any, text: string): Assertion {
  return deepEqualInternal(a, b, text, "root");
}

const trueAssertion = { passed: true, text: null };

function toString(val: any): string {
  return val === null
    ? "null"
    : val === undefined
      ? "undefined"
      : (val?.toString() ?? "object");
}

function falseAssertion(a: any, b: any, text: string, path: string): Assertion {
  return {
    passed: false,
    text: `${text}. At ${path} expected: ${toString(b)}, actual ${toString(a)}`
  };
}

export function deepEqualInternal(a: any, b: any, text: string, path: string): Assertion {
  if (a === b) { return trueAssertion; }
  if (a == null) { return b == null ? trueAssertion : falseAssertion(a, b, text, path); }
  if (Array.isArray(a)) { return arraysEqual(a, b, text, path); }
  if (typeof a === "object") { return objectsEqual(a, b, text, path); }
  return falseAssertion(a, b, text, path);
}

function arraysEqual(a: any[], b: any, text: string, path: string): Assertion {
  if (!Array.isArray(b)) {
    return {
      passed: false,
      text: `${text}. Array at ${path}, expected: ${toString(b)}`
    };
  }
  if (a.length !== b.length) {
    return {
      passed: false,
      text: `${text}. Array length at ${path}, expected ${b.length}, actual ${a.length}`
    };
  }
  const failing = a
    .map((item, index) => deepEqualInternal(item, b[index], text, `${path}[${index}]`))
    .find(i => !i.passed);
  return failing || trueAssertion;
}

function objectsEqual(a: any, b: any, text: string, path: string): Assertion {
  if (typeof b !== "object" || b == null) { return falseAssertion(a, b, text, path); }

  for (let key in a) {
    if (typeof a.hasOwnProperty !== "function" || a.hasOwnProperty(key)) {
      const field = deepEqualInternal(a[key], b[key], text, `${path}.${key}`);
      if (!field.passed) { return field; }
    }
  }

  if (typeof b.hasOwnProperty === "function") {
    for (let key in b) {
      if (typeof b.hasOwnProperty !== "function" || b.hasOwnProperty(key) && !(key in a)) {
        return {
          passed: false,
          text: `${text}. Expected field at ${path}.${key} is not present`
        };
      }
    }
  }

  return trueAssertion;
}
