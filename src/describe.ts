import { IExpect, Test } from "./types";
import { Tester } from "./collection/tester";
import { collector } from "./collection/collector";

export function test(name: string, action: (t: IExpect) => void | Promise<void>): void {
  describe("", it => it(name, action));
}

export type It = (name: string, action: (t: IExpect) => void | Promise<void>) => void;

export function describe(groupName: string, describeAction: (it: It) => void): void {
  async function it(name: string, itAction: (t: IExpect) => void | Promise<void>): Promise<void> {
    const token = {};
    collector.startTest(token);
    const tester = new Tester();

    try {
      const start = new Date();
      const waiter: any = itAction(tester);
      if (waiter?.then != null) {
        await waiter;
      }
      const end = new Date();
      const testSpan = end.getTime() - start.getTime();
      if (collector.options && collector.options.testTimeout > 0 && testSpan > collector.options.testTimeout) {
        tester.error(new Error(`Test took ${testSpan}`));
      }
    } catch (err) {
      tester.error(err);
    }
    const test: Test = {
      groupName,
      name,
      assertions: tester.assertions,
      passed: !tester.assertions.some(a => !a.passed)
    };
    collector.finishTest(token, test);
  }
  describeAction((n, a) => it(n, a));
}
