import { parentPort, workerData, Worker, isMainThread } from "worker_threads";
import { execute } from "./execute";
import { RunnerOptions } from "../runnerOptions.type";
import { Test } from "../types";

export function runWorker(files: string[], options: RunnerOptions): Promise<Test[]> {
  return new Promise((resolve, reject) => {
    const worker = new Worker(__filename, { workerData: { files, options } });
    worker.on("message", result => {
      if (Array.isArray(result)) { resolve(result); } else { reject(result); }
    });
    worker.on("error", err => {
      reject(err);
      worker.terminate();
    });
    worker.on("exit", (code) => {
      if (code !== 0) {
        reject(new Error(`Worker stopped with exit code ${code}`));
      }
    });
  });
}

if (!isMainThread) {
  const { files, options } = workerData;
  execute(files, options)
    .then(tests => parentPort.postMessage(tests))
    .catch(err => parentPort.postMessage(err));
}
