import { collector } from "../collection/collector";
import { RunnerOptions } from "../runnerOptions.type";
import { Test } from "../types";

export async function execute(files: string[], options: RunnerOptions): Promise<Test[]> {
  let rejecter = (err: any) => null;
  let rejected = false;
  collector.setOptions(options);
  collector.reset();
  const timer = setTimeout(() => { rejecter(new Error("Tests failed with timeout.")); rejected = true; }, options.timeout);
  if (files.length > 0) {
    await Promise.all(files.map(file => import(file)));
  }
  const tests = await new Promise<Test[]>((resolve, reject) => {
    rejecter = reject;
    collector.onFinish(results => {
      if (rejected) { return; }
      clearTimeout(timer);
      resolve(results);
    });
  });

  files.forEach(file => {
    delete require.cache[require.resolve(file)];
  });

  return tests;
}
