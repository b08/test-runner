import { RunnerOptions } from "../runnerOptions.type";
import { Test } from "../types";
import { execute } from "./execute";
import { splitInGroups } from "./splitInGroups";
import { runWorker } from "./workerThreadExecute";

export async function splitExecute(files: string[], options: RunnerOptions): Promise<Test[]> {
  if (files.length === 0) { return []; }
  const groups = splitInGroups(files, options.threads);
  const last = groups[groups.length - 1];
  const rest = groups.slice(0, groups.length - 1);
  const allTests = await Promise.all([...rest.map(grp => runWorker(grp, options)), execute(last, options)]);
  const result = allTests.reduce((acc, val) => [...acc, ...val], []);
  return result;
}

