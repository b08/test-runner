import * as fse from "fs-extra";
import { runTests } from "./runTests";
import { argv } from "yargs";
import { RunnerOptions } from "./runnerOptions.type";

const optionsFile = "test-runner.json";

let options: RunnerOptions = {};
if (fse.existsSync(optionsFile)) {
  try {
    const content = fse.readFileSync(optionsFile, "utf8");
    options = JSON.parse(content);
  } catch (err) {
    // do nothing
  }
}

options = { ...options, ...argv };
if (argv["_"].length > 0) { options.files = argv["_"]; }

runTests(options)
  .then(result => {
    console.log(result.output);
    if (!result.passed) {
      process.exitCode = -1;
    }
  })
  .catch((err) => {
    console.error(err);
    process.exitCode = -1;
  });
