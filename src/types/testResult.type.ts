export interface TestResult {
  passed: boolean;
  output: string;
}
