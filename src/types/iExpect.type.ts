export interface IExpect {
  true(value: boolean, customText?: string): void;
  false(value: boolean, customText?: string): void;
  equal<T>(value1: T, value2: T, customText?: string): void;
  deepEqual<T>(value1: T, value2: T, customText?: string): void;
  exception(code: () => void | Promise<void>, customText?: string): void | Promise<void>;
}
