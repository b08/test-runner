export interface Test {
  groupName: string;
  name: string;
  assertions: Assertion[];
  passed: boolean;
}

export interface Assertion {
  text: string;
  expected?: any;
  actual?: any;
  passed: boolean;
}
