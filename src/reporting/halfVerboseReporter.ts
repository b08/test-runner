import { IReporter } from "../runnerOptions.type";
import { Assertion, Test } from "../types";
import { redAssertion } from "./parts/assertionPrints";
import { colors } from "./parts/colors.const";
import { reportGroups } from "./parts/reportGroups";
import { greenTest, redTest } from "./parts/testPrints";

export const HalfVerboseReporter: IReporter = {
  report(tests: Test[]): string {
    return reportGroups(tests, reportTests);
  }
};

function reportTests(tests: Test[]): string {
  let result = "";
  for (const test of tests) {
    result += test.passed ? greenTest(test) : redTest(test);
    result += reportAssertions(test.assertions);
  }
  return result;
}

export function reportAssertions(assertions: Assertion[]): string {
  let result = "";
  const green = assertions.filter(a => a.passed);
  const red = assertions.filter(a => !a.passed);
  if (green.length > 0) {
    result += `      ${colors.green}√ ${green.length} assertion(s) passed${colors.reset}\n`;
  }
  for (const assertion of red) {
    result += redAssertion(assertion);
  }
  return result;
}
