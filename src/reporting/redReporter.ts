import { IReporter } from "../runnerOptions.type";
import { Test } from "../types";
import { reportGroups } from "./parts/reportGroups";
import { redTest } from "./parts/testPrints";
import { reportRedAssertions } from "./briefReporter";

export const RedReporter: IReporter = {
  report(tests: Test[]): string {
    return reportGroups(tests, reportTests);
  }
};

function reportTests(tests: Test[]): string {
  let result = "";
  for (const test of tests) {
    if (!test.passed) {
      result += redTest(test);
      result += reportRedAssertions(test.assertions);
    }
  }
  return result;
}
