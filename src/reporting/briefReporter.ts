import { IReporter } from "../runnerOptions.type";
import { Test, Assertion } from "../types";
import { reportGroups } from "./parts/reportGroups";
import { greenTest, redTest } from "./parts/testPrints";
import { redAssertion } from "./parts/assertionPrints";

export const BriefReporter: IReporter = {
  report(tests: Test[]): string {
    return reportGroups(tests, reportTests);
  }
};

function reportTests(tests: Test[]): string {
  let result = "";
  for (const test of tests) {
    if (test.passed) {
      result += greenTest(test);
    } else {
      result += redTest(test);
      result += reportRedAssertions(test.assertions);
    }
  }
  return result;
}

export function reportRedAssertions(assertions: Assertion[]): string {
  return assertions.filter(x => !x.passed).map(redAssertion).join("");
}
