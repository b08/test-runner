import { IReporter, ReporterOption } from "../runnerOptions.type";
import { BriefReporter } from "./briefReporter";
import { VerboseReporter } from "./verboseReporter";
import { RedReporter } from "./redReporter";
import { SummaryReporter } from "./summaryReporter";
import { HalfVerboseReporter } from "./halfVerboseReporter";

export function getReporters(reporters: ReporterOption | ReporterOption[]): IReporter[] {
  return Array.isArray(reporters)
    ? reporters.map(getReporter)
    : [getReporter(reporters)];
}

function getReporter(option: ReporterOption): IReporter {
  const reporter = <IReporter>option;
  if (typeof reporter.report === "function") { return reporter; }
  switch (option) {

    case "brief":
      return BriefReporter;
    case "red":
      return RedReporter;
    case "summary":
      return SummaryReporter;
    case "verbose":
      return VerboseReporter;
    case "silent":
      return { report: () => null };
    case "normal":
    default:
      return HalfVerboseReporter;
  }
}
