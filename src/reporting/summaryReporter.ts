import { IReporter } from "../runnerOptions.type";
import { Test } from "../types";
import { colors } from "./parts/colors.const";

export const SummaryReporter: IReporter = {
  report(tests: Test[]): string {
    let result = "";
    const passedTests = tests.filter(x => x.passed).length;
    const failedTests = tests.length - passedTests;
    let testLine = "";
    if (passedTests > 0) {
      testLine += `${colors.green}${passedTests} passed${colors.reset}, `;
    }
    if (failedTests > 0) {
      testLine += `${colors.red}${failedTests} failed${colors.reset}, `;
    }
    result += `Tests: ${testLine}${tests.length} total\n`;

    let passedAssertions = 0;
    let failedAssertions = 0;
    for (const test of tests) {
      for (const assertion of test.assertions) {
        if (assertion.passed) { passedAssertions++; } else { failedAssertions++; }
      }
    }
    const totalAssertions = passedAssertions + failedAssertions;

    let assertionLine = "";
    if (passedAssertions > 0) {
      assertionLine += `${colors.green}${passedAssertions} passed${colors.reset}, `;
    }

    if (failedAssertions > 0) {
      assertionLine += `${colors.red}${failedAssertions} failed${colors.reset}, `;
    }
    return result + `Assertions: ${assertionLine}${totalAssertions} total\n`;
  }
};
