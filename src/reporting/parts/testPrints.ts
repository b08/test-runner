import { colors } from "./colors.const";
import { Test } from "../../types/test.type";

export function greenTest(test: Test): string {
  return `  ${colors.green}@${colors.reset} ${test.name}\n`;
}

export function redTest(test: Test): string {
  return `  ${colors.red}@ ${test.name}${colors.reset}\n`;
}
