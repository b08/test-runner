import { Assertion } from "../../types";
import { colors } from "./colors.const";
import { printObject } from "./printObject";

export function printAssertions(assertions: Assertion[]): string {
  return assertions
    .map(a => a.passed ? greenAssertion(a) : redAssertion(a))
    .join("");
}

export function redAssertion(assertion: Assertion): string {
  const header = `      ${colors.red}${assertion.text}${colors.reset}\n`;
  if (!("actual" in assertion)) { return header; }
  return header + `Actual:
${printObject(assertion.actual)}
Expected:
${printObject(assertion.expected)}`;
}

export function greenAssertion(assertion: Assertion): string {
  return `    ${colors.green}√${colors.reset} ${assertion.text}\n`;
}

