import { colors } from "./colors.const";

export function printObject(object: any): string {
  const str = JSON.stringify(object, null, 2) + "\n";

  const colored = str.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
    function (match: string): string {
      let color = colors.yellow;
      if (/^"/.test(match)) {
        if (/:$/.test(match)) {
          color = colors.red;
        } else {
          color = colors.green;
        }
      } else if (/true|false/.test(match)) {
        color = colors.blue;
      } else if (/null/.test(match)) {
        color = colors.magenta;
      }
      return `${color}${match}${colors.reset}`;
    });
  const split = colored.split(/[\r\n]+/).filter(x => x.length > 0);
  if (split.length > 20) {
    return split.slice(0, 19).join("\n") + "\n...\n";
  }
  return colored;
}
