import { Test } from "../../types";

function groupTests(array: Test[], keySelector: (test: Test) => string): any {
  const result = Object.create(null);
  for (let item of array) {
    const key = keySelector(item);
    const list = result[key] || (result[key] = []);
    list.push(<any>item);
  }
  return result;
}

export function reportGroups(tests: Test[], reportTests: (tests: Test[]) => string): string {
  const grouped = groupTests(tests, test => test.groupName);
  let result = "";
  let needSpace = false;
  if (grouped[""]) {
    result += reportTests(grouped[""]);
    needSpace = true;
  }
  for (const group of Object.keys(grouped)) {
    if (group === "") {
      continue;
    }
    if (needSpace) { result += "\n"; }
    result += `  ${group}\n`;
    result += reportTests(grouped[group]);
    needSpace = true;
  }
  return result;
}
