export const colors = {
  red: "\x1b[91m",
  green: "\x1b[92m",
  reset: "\x1b[0m",
  yellow: "\x1b[33m",
  blue: "\x1b[94m",
  magenta: "\x1b[95m"
};
