import { IReporter } from "../runnerOptions.type";
import { Test, Assertion } from "../types";
import { reportGroups } from "./parts/reportGroups";
import { greenTest, redTest } from "./parts/testPrints";
import { printAssertions, redAssertion, greenAssertion } from "./parts/assertionPrints";

export const VerboseReporter: IReporter = {
  report(tests: Test[]): string {
    return reportGroups(tests, reportTests);
  }
};

function reportTests(tests: Test[]): string {
  let result = "";
  for (const test of tests) {
    result += test.passed ? greenTest(test) : redTest(test);
    result += reportAssertions(test.assertions);
  }
  return result;
}

export function reportAssertions(assertions: Assertion[], limit: number = 5): string {
  if (assertions.length <= limit) { return printAssertions(assertions); }
  let result = "";
  const failed = assertions.filter(x => !x.passed);
  let successfulLimit = limit - failed.length - 1;
  let printDots = false;
  for (const assertion of assertions) {
    if (!assertion.passed) {
      result += redAssertion(assertion);
      continue;
    }
    if (successfulLimit > 0) {
      result += greenAssertion(assertion);
      successfulLimit--;
    } else {
      printDots = true;
    }
  }
  if (printDots && failed.length <= limit) {
    result += "      ...\n";
  }

  return result;
}
