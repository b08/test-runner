import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { passingAssertions, passingTests, totalAssertions, totalTests } from "./regex";
import { passingTest } from "./testPath";

test("should pass 2 tests with 2 assertions", async expect => {
  // arrange

  // act
  const testResult = await runTests({
    files: passingTest,
    reporter: ["summary"]
  });
  const summary = testResult.output;

  // assert
  expect.true(testResult.passed);
  expect.equal(passingTests(summary), 2);
  expect.equal(totalTests(summary), 2);

  expect.equal(passingAssertions(summary), 4);
  expect.equal(totalAssertions(summary), 4);

});
