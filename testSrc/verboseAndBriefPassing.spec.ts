import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { passingTest } from "./testPath";
import { colors } from "../src/reporting/parts/colors.const";

test("verbose and brief should output lots of green stuff", async expect => {
  // arrange

  // act

  const testResult = await runTests({
    files: passingTest,
    reporter: ["brief", "verbose"]
  });
  const testText = testResult.output;

  // assert
  expect.true(testResult.passed);
  expect.true(testText.length > 100);
  expect.true(testText.includes(colors.green));
  expect.false(testText.includes(colors.red));
});

