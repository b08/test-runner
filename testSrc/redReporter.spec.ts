import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { passingTest } from "./testPath";

test("red reporter should output nothing for green tests", async expect => {
  // arrange

  // act

  const testResult = await runTests({
    files: passingTest,
    reporter: ["red"]
  });
  const testText = testResult.output;

  // assert
  expect.true(testResult.passed);
  expect.true(testText.trim().length === 0);
});
