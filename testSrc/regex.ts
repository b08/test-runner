
export function passingTests(summary: string): number {
  const match = summary.match(/Tests:[^\n]*[^\d](\d+) passed/);
  return match && +match[1];
}

export function failingTests(summary: string): number {
  const match = summary.match(/Tests:[^\n]*[^\d](\d+) failed/);
  return match && +match[1];
}

export function totalTests(summary: string): number {
  const match = summary.match(/Tests:[^\n]*[^\d](\d+) total/);
  return match && +match[1];
}

export function passingAssertions(summary: string): number {
  const match = summary.match(/Assertions:[^\n]*[^\d](\d+) passed/);
  return match && +match[1];
}

export function failingAssertions(summary: string): number {
  const match = summary.match(/Assertions:[^\n]*[^\d](\d+) failed/);
  return match && +match[1];
}

export function totalAssertions(summary: string): number {
  const match = summary.match(/Assertions:[^\n]*[^\d](\d+) total/);
  return match && +match[1];
}

