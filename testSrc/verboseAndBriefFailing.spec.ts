import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { colors } from "../src/reporting/parts/colors.const";
import { failingTest } from "./testPath";

test("verbose and brief should output lots of red stuff", async expect => {
  // arrange

  // act
  const testResult = await runTests({
    files: failingTest,
    reporter: ["brief", "verbose"]
  });
  const testText = testResult.output;

  // assert
  expect.false(testResult.passed);
  expect.true(testText.length > 100);
  expect.true(testText.includes(colors.red));
});
