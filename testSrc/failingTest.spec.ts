import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { failingAssertions, failingTests, totalAssertions, totalTests } from "./regex";
import { failingTest } from "./testPath";

test("should fail 2 tests with 9 assertions", async expect => {
  // arrange

  // act
  const testResult = await runTests({
    files: failingTest,
    reporter: ["summary"]
  });
  const summary = testResult.output;

  // assert
  expect.false(testResult.passed);
  expect.equal(failingTests(summary), 2);
  expect.equal(totalTests(summary), 2);

  expect.equal(failingAssertions(summary), 9);
  expect.equal(totalAssertions(summary), 9);
});
