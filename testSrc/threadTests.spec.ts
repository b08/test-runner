import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { failingAssertions, failingTests, passingAssertions, passingTests, totalAssertions, totalTests } from "./regex";
import { failingTest, passingTest } from "./testPath";

test("should work with 2 threads", async expect => {
  // arrange

  // act
  const testResult = await runTests({
    files: [failingTest, passingTest],
    reporter: ["summary"],
    threads: 2
  });
  const summary = testResult.output;

  // assert
  expect.false(testResult.passed);
  expect.equal(passingTests(summary), 2);
  expect.equal(failingTests(summary), 2);
  expect.equal(totalTests(summary), 4);

  expect.equal(passingAssertions(summary), 4);
  expect.equal(failingAssertions(summary), 9);
  expect.equal(totalAssertions(summary), 13);
});
