import { test } from "@b08/test-runner";
import { runTests } from "../src";
import { failingAssertions, failingTests, totalAssertions, totalTests } from "./regex";
import { exceptionTest } from "./testPath";

test("should work with exceptions in the tests", async expect => {
  // arrange

  // act
  const testResult = await runTests({
    files: exceptionTest,
    reporter: ["summary"]
  });
  const summary = testResult.output;

  // assert
  expect.false(testResult.passed);
  expect.equal(failingTests(summary), 2);
  expect.equal(totalTests(summary), 5);

  expect.equal(failingAssertions(summary), 2);
  expect.equal(totalAssertions(summary), 5);
});
